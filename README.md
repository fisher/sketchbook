# sketchbook
sketchbook for Arduino

## language reference links

* [official reference guide][1]

## hardware reference links

* [NRF24l01][2]

## using LCD nokia5110

I've got this fine piece of old machinery called Nokia 5110 screen; it's black-n-white lcd screen with led backlight of various colors, 84x48 dots resolution, using PCD8544 controller and works on SPI. Here is an example usage.

Pins on the screen PCB are: RST, CE, DC, DIN, CLK, Vcc, BL, GND

Pins 1-5 goes to digital 3-7 of the arduino, through 10k resistors.

Vcc is simply 3.3v from the Arduino.

BL is a backlight leds, goes to _ground_ on Arduino, through 0k33 .. 2k0 resistor.

GND is ground.

## using OLED 128x64 display through I2C

SCL -> A5
SDA -> A4
Vcc -> 3.3v
GND -> GND

The source taken from the example, the only thing needed to tweak is the line with display.begin

It should init 0x3C instead of 0x3D

## S4A Firmware

should be compiled and uploaded to UNO board in order to get it working from within Scratch4Arduino project

## control_face

this is a small project for playing with my kids; controlling an image with 4 directional keys

## i2c_128x64_DHT22

DHT22 sensor reading and writing the values to LCD display.

## side-note

there is also a possibility to work with arduino hardware directly, without any
java helpers in the middle (I look at you, arduino IDE). Will be more complex
but more predictable, [starting point][3].



[1]: https://www.arduino.cc/reference/en/
[2]: https://howtomechatronics.com/tutorials/arduino/arduino-wireless-communication-nrf24l01-tutorial/
[3]: http://www.florentflament.com/blog/arduino-hello-world-without-ide.html

