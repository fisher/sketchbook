int rawvalue[6];
int millivolt[6];

void setup()
{
  Serial.begin(9600);
  Serial.println("Measuring voltage");
  Serial.println("The readings:");
}

void loop()
{
  float temp;
  int i;
  
  // read in a loop
  for (i=0; i<6; i++) rawvalue[i] = analogRead(i);
  Serial.print("Raw values");

  // calc value
  for (i=0; i<6; i++) {
    temp = rawvalue[i] /4.092;
    millivolt[i] = (int) (temp *100);
  }

  // output loop
  for (i=0; i<6; i++) {
    Serial.print(", ");
    Serial.print(i);
    Serial.print(":"); 
    Serial.print(rawvalue[i]);
  }

  Serial.println(".");

  delay(1000);
}

