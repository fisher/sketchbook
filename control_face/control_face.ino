/*********************************************************************
This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

This example is for a 128x64 size display using I2C to communicate
3 pins are required to interface (2 I2C and one reset)

Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.  
BSD license, check license.txt for more information
All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 

static const unsigned char PROGMEM ship[] =

{
    B00001000, B00010000,
    B00010100, B00101000,
    B00010111, B11101000,
    B00011000, B00011000,
    B00100010, B01000100,
    B00100010, B01000100,
    B00100010, B01000100,
    B00100010, B01000100,
    B00010000, B00001000,
    B00010010, B01001000,
    B00001001, B10010000,
    B00000110, B01100000,
    B00000011, B11000000,
    B00000000, B00000000,
    B00000000, B00000000,
    B00000000, B00000000,
};

static const unsigned char PROGMEM ship_up[] =

{
    B00001000, B00010000,
    B00010100, B00101000,
    B00010111, B11101000,
    B00011000, B00011000,
    B00100010, B01000100,
    B00100010, B01000100,
    B00100010, B01000100,
    B00100010, B01000100,
    B00010000, B00001000,
    B00010010, B01001000,
    B00001001, B10010000,
    B00000110, B01100000,
    B00000011, B11000000,
    B00000010, B01000000,
    B00000101, B10100000,
    B00000010, B01000000,
};

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

const int left = 2;
const int up = 5;
const int down = 7;
const int right = 8;

#define maxdelta 8

int leftState = 0;
int upState = 0;
int downState = 0;
int rightState = 0;

void setup()   {
  Serial.begin(9600);

  pinMode(left, INPUT);
  pinMode(up, INPUT);
  pinMode(down, INPUT);
  pinMode(right, INPUT);

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C  );  // initialize with the I2C addr 0x3D (for the 128x64)
  // init done

  // Clear the buffer.
  display.clearDisplay();

  // draw a bitmap icon and 'animate' movement
  testdrawbitmap(ship, LOGO16_GLCD_HEIGHT, LOGO16_GLCD_WIDTH);
}


void loop() {
  
}


void testdrawbitmap(const uint8_t *bitmap, uint8_t w, uint8_t h) {
  int xpos, ypos;
  int xdelta, ydelta;
  int xborder, yborder;

  // initialize
  xpos = random(30) + 10;
  ypos = random(30) + 10;
  xdelta = random(10) -5;
  ydelta = random(10) -5;
  xborder = display.width() - w;
  yborder = display.height() - h;

  while (1) {
    leftState = digitalRead(left);
    upState = digitalRead(up);
    downState = digitalRead(down);
    rightState = digitalRead(right);

    // debug it
    //Serial.print("x: ");
    //Serial.print(xpos, DEC);
    //Serial.print(" y: ");
    //Serial.print(ypos, DEC);

    if (upState == HIGH) {
      // draw it with fire
      display.drawBitmap(xpos, ypos, ship_up, w, h, WHITE);

      // display it
      display.display();
      delay(100);
    
      // then erase it + move it
      display.drawBitmap(xpos, ypos, ship_up, w, h, BLACK);
    } else {
      // draw it
      display.drawBitmap(xpos, ypos, ship, w, h, WHITE);

      // display it
      display.display();
      delay(100);
    
      // then erase it + move it
      display.drawBitmap(xpos, ypos, ship, w, h, BLACK);
    }

    if (xdelta >0) { xdelta--; }
    if (xdelta <0) { xdelta++; }
    if (ydelta >1) { ydelta--; }
    if (ydelta <1) { ydelta++; }

    if (downState == HIGH) {
        ydelta += 2;
    }
    if (rightState == HIGH) {
        xdelta += 2;
    }
    if (upState == HIGH) {
        ydelta -= 2;
    }
    if (leftState == HIGH) {
        xdelta -= 2;
    }

    if (xdelta > maxdelta) { xdelta = maxdelta; }
    if (xdelta < -maxdelta) { xdelta = -maxdelta; }
    if (ydelta > maxdelta) { ydelta = maxdelta; }
    if (ydelta < -maxdelta) { ydelta = -maxdelta; }

    ypos += ydelta;
    xpos += xdelta;

    // check the borders
    if (xpos > xborder){
        xpos = xborder;
    }
    if (ypos > yborder){
        ypos = yborder;
    }
    if (xpos < 0){
        xpos = 0;
    }
    if (ypos < 0){
        ypos = 0;
    }

  }
}
